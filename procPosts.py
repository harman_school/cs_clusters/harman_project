
# %%
import pandas as pd
import sys
import os
from getpass import getuser
import sys

# Turn off this stupid warning
pd.options.mode.chained_assignment = None

############################################################################
# Function definitions
############################################################################

def loadTitles(filePath_orig = 'swTitleFull.csv', filePath_mod = 'swLabeled.csv'):

    """ Load all titiles or existing label file if it exists"""

    if os.path.isfile(filePath_mod):
        print(f'loading existing label file {filePath_mod}...\n')
        
        # Verify that file path exists
        assert os.path.isfile(filePath_mod), (
            print(f"File {filePath_mod} does not exist..."))

        return pd.read_csv(filePath_mod)

    else:
        print('loading original title file...\n')

        # Verify that file path exists
        assert os.path.isfile(filePath_orig), (
            print(f"File {filePath_orig} does not exist..."))

        # Load and add label items
        return pd.read_csv(filePath_orig)

def printUsage():

    """ Print usage for this script """

    msg = """

    Post Labeling: Please enter the post type followed by the post subtype, post types listed below,

    Suicide Types:
    1) Passive Ideation: I've thought about what it would be like to be dead
    2) Active Ideation: I've thought about how I would commit suicide
    3) Past Attempt: I tried last week/night/etc
    4) Future Attempt: I will kill myself tonight
    5) In Need of Help: Please someone help me
    6) Second Hand: A friend or loved one attempted or has strong ideation 

    Other:
    1) Trauma: Abuse / or traumatic event (divorce, assault, etc)
    2) Relational Support: "Anyone else..." (shared pain)
    3) Optimistic Support: It gets better / I decided to live
    4) Negative Situation: Everything is going wrong, they hung up on me
    5) Negative User Post : Everyone needs to quit complaining
    6) Other Misc: Sometimes I eat catfood

    Enter: Post type, enter suicide (s), other (o), or unable to determine (amb)
    Enter: Number for post subtype (not required for amb)
    Enter: h at any time to repeat this message
    Enter: verbose to toggle printing subtype each time

    """

    print(msg, end = '\r')

def backupDF(df):
    """ Save a backup copy of the .csv """
    df.to_csv('.backup_swLabeled.csv', index = False)

def returnUser(df):
    """ Get username """

    # To store values under appropriate user
    userName = 'Hope' #getuser()
    userCol = 'postLabel_' + userName

    # Add new column for grading if user grading doesnt exist
    if userCol not in list(df.columns):
        print(f'Welcome {userName}; Adding new grading schema\n')
        df[userCol] = ['-'] * df.shape[0]
    
    else:
        print(f'Welcome back {userName}; Using your existing grading schema\n')

    return df, userName, userCol


class PresentItem:

    def __init__(self, itm):
        """ Initialize params """
        self.itm = itm

    def wipeConsole(self):
        # Flush the terminal
        os.system('clear')     

    def printHelp(self): 
        printUsage()

    def printSubOptions(self, postType):
        """ Print subtype options """

        if postType == "s":
            msg = """
            Suicide Types:
            1) Passive Ideation: I've thought about what it would be like to be dead
            2) Active Ideation: I've thought about how I would commit suicide
            3) Past Attempt: I tried last week/night/etc
            4) Future Attempt: I will kill myself tonight
            5) In Need of Help: Please someone help me
            6) Second Hand: A friend or loved one attempted or has strong ideation 

            """
        else:
            msg = """
            Other:
            1) Trauma: Abuse / or traumatic event (divorce, assault, etc)
            2) Relational Support: "Anyone else..." (shared pain)
            3) Optimistic Support: It gets better / I decided to live
            4) Negative Situation: Everything is going wrong, they hung up on me
            5) Negative User Post : Everyone needs to quit complaining
            6) Other Misc: Sometimes I eat catfood

            """
        
        print(msg, end = '\r')
    
    def convertKeyToLabel(self, itm1, itm2):
        
        subKey = {'s': {'1': 'passiveIdeation',
                        '2': 'activeIdeation',
                        '3': 'pastAttempt',
                        '4': 'futureAttempt',
                        '5': 'needHelp',
                        '6': 'secondHand'},
                   'o': {'1': 'trauma',
                        '2': 'sharedMisery',
                        '3': 'optimisticSupport',
                        '4': 'negativeSituation',
                        '5': 'negativeUser',
                        '6': 'otherMisc'}}

        return itm1 + '_' + subKey[itm1][itm2]

    def getFirstReponse(self):
        """ Get post type from user """

        # Present Item and get response
        self.respType = input(f"{self.itm} \n Post Type; Suicide (s) or Other (o): ").lower()

        while self.respType not in ['s', 'o', 'amb']:

            # Check for exit code 
            if self.respType in ['q', 'quit', 'exit']: 
                return None

            else:
                self.wipeConsole()
                print(f'Invalid Response: {self.respType}')
                printUsage()
                self.respType = input(f"{self.itm} \n Post Type; Suicide (s) or Other (o): ").lower()

        return self.respType
    
    def getSecondResponse(self):
        """ Get post subtype from user """

        # Make sure the first response has been called
        assert hasattr(self,'respType'), (
            print('Have not called first response yet somehow...'))

        # Get list of options based on first response
        listOpts = ['1', '2', '3', '4', '5', '6'] if self.respType == 's' else ['1', '2', '3', '4', '5', '6']

        self.printSubOptions(self.respType)

        self.respSubType = input(f"Enter Subtype: ").lower()

        while self.respSubType not in listOpts:
            
            # Check for exit code 
            if self.respSubType in ['q', 'quit', 'exit']: 
                return None

            else:
                print(f'Invalid option: {self.respSubType}') 
                self.printSubOptions(self.respType)
                self.respSubType = input(f"Enter Subtype: ").lower()

        return self.respSubType

    def runExitSeq(self, df, counts, exit = True, outFile = 'swLabeled.csv'):
        """ Run the exit sequence and save the output """ 
        
        self.wipeConsole()

        # Overwrite uptdated output
        print('Writing updated label output...')
        df.to_csv(outFile, index = False)

        print(f'Thanks {getuser()}, you labeled: {counts} posts')

        # Exit
        if exit: sys.exit('Leaving script...')

    def runMain(self, df, counts):
        """ Run the main sequence """

        # Get the first response
        self.firstResponse = self.getFirstReponse()

        # Run exit seq
        if self.firstResponse == None:
            self.runExitSeq(df, counts)

        # Return ambiguous post
        elif self.firstResponse == 'amb':
            self.wipeConsole()

            return self.firstResponse

        # Get the second response
        self.secondResponse = self.getSecondResponse()
        
        if self.secondResponse == None:
            self.runExitSeq(df, counts)

        self.wipeConsole()

        return self.convertKeyToLabel(self.firstResponse, self.secondResponse)


############################################################################
# RUN
############################################################################
# %%

if __name__ == "__main__":

    # Load the data and store a backup copy
    df = loadTitles(); backupDF(df)

    # Parse username and update df
    df, currUser, userCol = returnUser(df)
    print(currUser, userCol)
    
    # Set counter
    counter = 0

    # Loop through all posts
    for ind in range(df.shape[0]):

        # Check that title does not already have label
        if df[userCol][ind] == '-':
            # Retrieve label
            print(df[userCol][ind])
            currItm = PresentItem(df['title'][ind])

            try: # to add item 
                resp = currItm.runMain(df, counter)

                # Update response items
                df[userCol][ind] = resp

                # Add to the amount of graded items
                counter += 1
            
            # On KeyboardInterrupt save output
            except KeyboardInterrupt:
                currItm.runExitSeq(df, counter, False)
                raise

