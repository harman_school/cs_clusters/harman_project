#!/usr/bin/python3

import json
import numpy as np
# from pyspark import SparkContext, SparkConf
import pickle

#########################################################################
# Setup spark
#########################################################################

# conf = SparkConf().setAppName("redditProc")
# sc = SparkContext(conf = conf)

#########################################################################
# Functions
#########################################################################

def jsonLoad(x):
    """Try and load each entry as a dictionary"""
    try: return json.loads(x)
    except: pass

def retAllScores(x):
    return int(x['score'])

def reNumComments(x):
    return int(x['num_comments'])

#########################################################################
# RUN
#########################################################################

#if __name__ == "__main__":

# Load data
data = sc.textFile('/data/reddit_submissions/')

# Broadcast Functions
sc.broadcast(jsonLoad)
sc.broadcast(retAllScores)
sc.broadcast(jsonLoad)

# Convert to dict
data = data.map(jsonLoad)

# Run mass filter
swPosts = data.filter(lambda x: (
                            type(x) == dict and
                            'subreddit' in x.keys() and 'title' in x.keys() and
                            x['subreddit'] == 'SuicideWatch' and
                            x['selftext'] not in ['[deleted]', '', '[removed]']
                            ))

# Create (postID, score) and (postID, num_comments) remove commas!
swScores = swPosts.map(lambda x: (int(x['score']), (x['id'], x['title'].replace(',', ''), x['num_comments'])))

# Sort by value
swScoreSort = swScores.sortByKey(False).collect()

# Store for pickling
pklDict = {'var': swScoreSort}

# Write as a pickle just in case
with open('redditOut.pkl', 'wb') as handle:
    pickle.dump(pklDict, handle, protocol=pickle.HIGHEST_PROTOCOL)

countFailed = 0

# Write output
with open('swTitleFull.csv', 'w') as f:
    # Write header
    f.write('id,score,numComments,title\n')
    for ind, post in enumerate(swScoreSort):
        if len(post) == 2:
            if type(post[1]) == tuple:
                if len(post[1]) == 3:
                    try:
                        txtOut = ''.join([i if ord(i) < 128 else ' ' for i in post[1][1]])
                        f.write(post[1][0] + ',' +
                                str(post[0]) + ',' +
                                str(post[1][2]) + ',' + 
                                txtOut + '\n')
                    except:
                        print('FAILED: {}'.format(post[1][1].encode('UTF-8')))
                        countFailed += 1

#swNumCommSort = swNumComm.sortByKey().collect()

######################################################

# Clean up the text and return counts
# swTitleClean = swTitles.map(applyTxtMap)
# swCounts = swTitleClean.map(mapCounts)

# # Make it smaller for testing
# swFilt = swCounts.filter(lambda x: len(x) > 20)

# # Flatten and get final counts
# swCountsFlat = swCounts.flatMap(lambda x: x)
# allFinal = swCountsFlat.reduceByKey(lambda x, n: x + n)

# for ii in oo[0].keys():
#     print('{} {}'.format(ii, oo[0][ii]))


# with open('temp.out', 'w') as f:
#     for ii in range(len(out)):
#         f.write('Author: ' + out[ii]['author'] + '\n')

# Add this change to see if it works 

# Replace usernames
#swPost = swPosts.map(lambda x: x['user_id'] = genString)

#puncs = str.maketrans('', '', string.punctuation)


###########################################################
# Function to return a random string of len
###########################################################

# def genString(l = 8):

#     opts = string.digits + string.ascii_lowercase + string.ascii_uppercase
#     outStr = ''.join([random.choice(opts) for x in range(l)])
    
#     return outStr
