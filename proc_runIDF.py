
import numpy as np
from collections import OrderedDict
from nltk.corpus import stopwords
import re
import sys
from math import log
from datetime import datetime
import os

def repRange(n, rep):
    """ Rep range returns multiple items from a range call """ 
    outRange = []
    for ii in range(n):
        outRange.append([ii] * rep)
    
    return [x for y in outRange for x in y]

def loadStop():
    """Load nltk stopwords"""
    # Set stopwords
    stop_words = set(stopwords.words('english')) 
    stop_words = [re.sub('[^A-Za-z0-9]+', '', x) for x in stop_words]
    stop_words.append('')
    return stop_words

def nlpProc(x, stop_words):
    """Text cleaning and return numpy count arrays"""
    # Retain only alphanumeric, split on whitespace and remove stopwords
    x = re.sub('[^A-Za-z0-9]+', ' ', x.lower())
    x = x.split(' ')
    x = [y for y in x if y not in stop_words]
    # Convert to numpy array and return unqie counts
    x = np.array(x)
    x = np.transpose(np.unique(x, return_counts=True))
    x = [tuple(ii) for ii in x]
    return x

def loadLabeledPosts(stop_words, n, mult, filePath = 'swLabeled.csv'):
    # Open the textfile
    with open(filePath) as f:
        content = [x.strip() for x in f.readlines()]
    # Take top N posts
    if mult > 1:
        topN = content * mult
    else:
        topN = content[1:n]
    print('Num Post: {}'.format(len(topN)))
    return topN

def procFreq(topN):
    stop_words = loadStop()
    if type(topN) != list:
        postID = topN.split(',')[0] 
        score = topN.split(',')[1] 
        numComms = topN.split(',')[2]
        origTitle = topN.split(',')[3]
        procTitle = nlpProc(topN.split(',')[3], stop_words)
        labels = topN.split(',')[4]
        nWords = np.sum(np.array([n for x, n in procTitle], dtype = float))
        finishedWords = [(x, round(float(n)/nWords, 4)) for x, n in procTitle]
        procTitle = finishedWords
        return (postID, score, numComms, origTitle, procTitle, labels)
    else:
        postID = [x.split(',')[0] for x in topN]
        score = [x.split(',')[1] for x in topN]
        numComms = [x.split(',')[2] for x in topN]
        origTitle = [x.split(',')[3] for x in topN]
        procTitle = [nlpProc(x.split(',')[3], stop_words) for x in topN]
        labels = [x.split(',')[4] for x in topN]
        for ind, _ in enumerate(procTitle):
            nWords = np.sum(np.array([n for x, n in procTitle[ind]], dtype = float))
            finishedWords = [(x, round(float(n)/nWords, 4)) for x, n in procTitle[ind]]
            procTitle[ind] = finishedWords
        out = zip(postID, score, numComms, origTitle, procTitle, labels)
        return out

# Hardcoding the number of numDocs so I dont forget
def returnIDF(numDocs = 26528, retNp = True, filePath = 'wordCountAll.csv'):
    """ Load the csv of corpus word counts and return a dictionary object """
    # Open and return each line
    with open('wordCountAll.csv') as f:
        content = [x.strip() for x in f.readlines()]
    # Store each word as key and count as value in a dict
    words = OrderedDict()
    # Skip header
    if retNp:
        idfWords = np.array([x.split(',')[-1] for x in content[1:]])
        idfValue = np.array([log((numDocs/float(x.split(',')[0])), 10) for x in content[1:]])
        return idfWords, idfValue
    else:
        for ii in content[1:]: # Get log(N/df_i)
            words[ii.split(',')[-1]] = log((numDocs/float(ii.split(',')[0])), 10)
        return words 

def procTF_IDF(s, idfWords, idfValues):
    """ Calculate IDF for a given list of tuples containg tf 
            Return: TF-IDF vector"""
    vOut = np.zeros(len(idfWords))
    # Get indices of words in this document in IDF (transpose and unnest)
    tInds = np.transpose(np.array([np.where(x[0] == idfWords)[0] for x in s]))[0]
    # Store TF * IDF in the appropiate index in the vOut vector 
    for ind, ii in enumerate(s):
        vOut[tInds[ind]] = idfValues[tInds[ind]] * ii[1]
    return vOut

def gatherAll(x, idfWords, idfValues):
    try: 
        return (x[0], x[1], procTF_IDF(x[2], idfWords, idfValues))
    except:
        return None #(x[0], x[1], [0, 1])


def runNonSpark(n, mult):
    """ Run the pipeline using nonspark data """
    tic = datetime.now() # Startime
    # Load stop words 
    stop_words = loadStop()
    # Return words and IDF values
    idfWords, idfValues = returnIDF()
    # Load SW titles
    swPosts = loadLabeledPosts(stop_words, n, mult)
    swPostsProc = procFreq(swPosts)
    # Create (id, classLabel, tf-idf vector)
    out = [(x[0], x[-1], x[4]) for x in swPostsProc]
    # Make sure length is correct
    outFilt = [x for x in out if len(x) == 3]
    # Get the final tfidf vectors 
    tfidfValues = [gatherAll(x, idfWords, idfValues) for x in outFilt]
    tfidfValuesFilt = [x for x in tfidfValues if x != None]
    # Write the output (necessary to compare times)
    print('Writing output')
    fxWriteOut(idfWords, tfidfValuesFilt, outFile = 'nativeTFIDF.vec')
    toc = datetime.now() - tic
    return ','.join(['nonSpark', str(n * mult), str(toc.seconds)])    

def toCSV1(posts): return posts[0] + ',' + ','.join(posts[1])
def toCSV2(posts): 
    try:
        outArr = ','.join(np.char.mod('%f', posts[-1]))
        return posts[0] + ',' + outArr
    except:
        return ','.join(['-1.00'] * 7330)

def runSpark(n, mult, sparkOut, time = True):
    """ Run the pipeline using the spark implementation """  
    tic = datetime.now() # Startime
    # Load stop words 
    stop_words = loadStop(); sc.broadcast(stop_words)
    # Return words and IDF values
    idfWords, idfValues = returnIDF()
    # Load SW titles
    swPosts = loadLabeledPosts(stop_words, n, mult)
    # Turn into an RDD
    postsPar = sc.parallelize(swPosts)
    swPostsProc = postsPar.map(lambda x: procFreq(x))
    # Create (id, classLabel, tf-idf vector)
    out = swPostsProc.map(lambda x: (x[0], x[-1], x[4]))
    #out = out.filter(lambda x: len(out) == 3)
    #out = out.filter(lambda x: None not in x)
    # Add header of words and write out
    idfWord = np.insert(idfWords, 0, 'id')
    idfWord = sc.parallelize([(idfWord[0], np.array(idfWord[1:]))])
    # Gather and return vector
    finalMap = out.map(lambda x: gatherAll(x, idfWords, idfValues))
    # Cast words and tfidf to csv
    wordFinal = idfWord.map(lambda x: toCSV1(x))
    vecFinal = finalMap.map(lambda x: toCSV2(x))
    # Combine these two
    finalOut = wordFinal.union(vecFinal)
    # Write the output
    #r = finalOut.collect()
    os.system('hadoop fs -rm -r ' + sparkOut)
    finalOut.saveAsTextFile(sparkOut)
    toc = datetime.now() - tic
    #os.system('hadoop fs -rm -r ' + sparkOut)
    return ','.join(['spark', str(n * mult), str(toc.seconds)])
    
def fxWriteOut(idfWords, vec, outFile = 'temp.out'):
    """ Function to write out the tfidf vector """
    failed = 0 # Only one fails
    # Write the output
    with open(outFile, 'w') as f:
        f.write('id,' + ','.join(idfWords) + '\n')
        for ii in vec:
            try:
                f.write(ii[0] + ',' + ','.join(np.array(ii[-1], dtype = str)) + '\n')
            except:
                failed += 1
    print('Failed: {}'.format(failed))
    
    
if __name__ == "__main__":
    
    """ RUN MAIN """

    writeOut = False # Choose to write output
    n = [10, 100, 250, 1000, 5000, 10000, 15000, 20000, 25000] # Number of titles to parse
    mult = [2, 4, 8] #, 16, 32]
    timeArr = []

    # Time non spark implementation
    for ii in n:
        timeArr.append(runNonSpark(ii, 1))
    for ii in mult:
        timeArr.append(runNonSpark(26528, ii))

    # If on linux (bigbird0) run spark implementation
    #if sys.platform != 'darwin':
    for ii in n:
        # Delete the current hadoop directory
        #os.system('hadoop fs -rm -r hdfs:///user/harmang/final_project/sparkOut.csv')
        try:
            sparkout = 'hdfs:///user/harmang/final_project/sparkOut_' + str(ii)
            timeArr.append(runSpark(ii, 1, sparkout))
        except:
            print('Failed: {}'.format(ii))
    for ii in mult:
        try:
            sparkout = 'hdfs:///user/harmang/final_project/sparkOut_' + str(26528 * ii)
            timeArr.append(runSpark(26528, ii, sparkout))
        except:
            print('Failed: {}'.format(ii))


    # Write the output
    with open('timeStats_native.csv', 'w') as f:
        f.write('alg,docs,time\n')
        [f.write(x + '\n') for x in timeArr]

# Collect the final output
sparkout = 'hdfs:///user/harmang/final_project/sparkOut_' + str(1005)
runNonSpark(1005, 1)
runSpark(1005, 1, sparkout)

3436 sw 1st ave apt 1 97239

hadoop fs -cat /user/harmang/final_project/sparkOut_1005/part-* > /cslu/homes/harmang/harman_project/sparkTFIDF.vec