#!/usr/bin/python3

import json
import string 
import numpy as np
import nltk 
import random
# from pyspark import SparkContext, SparkConf
from nltk.corpus import stopwords

# Setup spark
conf = SparkConf().setAppName("redditProc")
sc = SparkContext(conf = conf)


def jsonLoad(x):
    """Try and load each entry as a dictionary"""
    try: return json.loads(x)
    except: pass

def loadStop():
    """Load nltk stopwords"""
    # Set stopwords
    stop_words = set(stopwords.words('english')) 
    stop_words = [re.sub('[^A-Za-z0-9]+', '', x) for x in stop_words]
    return stop_words.append('')

def applyTxtMap(x):
    """Text cleaning and return numpy count arrays"""
    # Retain only alphanumeric, split on whitespace and remove stopwords
    x = re.sub('[^A-Za-z0-9]+', ' ', x.lower())
    x = x.split(' ')
    x = [y for y in x if y not in stop_words]
    # Convert to numpy array and return unqie counts
    x = np.array(x)
    x = np.transpose(np.unique(x, return_counts=True))
    return x

def mapCounts(x):
    """Return reduce the individual counts to group counts"""
    x = [(y, int(n)) for y, n in x]
    x = [(y, round(float(n)/len(x), 4)) for y, n in x]
    x = [(str(y), int(1)) for y, n in x]
    return x

def retAllScores(x):
    return int(x['score'])

def reNumComments(x):
    return int(x['num_comments'])

# load stopwords
stop_words = loadStop()

# Load data
data = sc.textFile('/data/reddit_submissions/')

# Convert to dict
data = data.map(jsonLoad)

# Check it is dict and has correct items
data = data.filter(lambda x: type(x) == dict)
data = data.filter(lambda x: 'subreddit' in x.keys() and 'title' in x.keys())

# Retain only SW posts and filter delected or removed and return title
swPosts = data.filter(lambda x: x['subreddit'] == 'SuicideWatch')
swPosts = swPosts.filter(lambda x: x['selftext'] not in ['[deleted]', '', '[removed]'])
swTitles = swPosts.map(lambda x: x['title'])

swScores = swPosts.map(retAllScores)
swNumCom = swPosts.map(retAllScores)

# Clean up the text and return counts
swTitleClean = swTitles.map(applyTxtMap)
swCounts = swTitleClean.map(mapCounts)

# Make it smaller for testing
swFilt = swCounts.filter(lambda x: len(x) > 20)

# Flatten and get final counts
swCountsFlat = swCounts.flatMap(lambda x: x)
allFinal = swCountsFlat.reduceByKey(lambda x, n: x + n)

for ii in oo[0].keys():
    print('{} {}'.format(ii, oo[0][ii]))


# with open('temp.out', 'w') as f:
#     for ii in range(len(out)):
#         f.write('Author: ' + out[ii]['author'] + '\n')

# Add this change to see if it works 

# Replace usernames
#swPost = swPosts.map(lambda x: x['user_id'] = genString)

#puncs = str.maketrans('', '', string.punctuation)


###########################################################
# Function to return a random string of len
###########################################################

# def genString(l = 8):

#     opts = string.digits + string.ascii_lowercase + string.ascii_uppercase
#     outStr = ''.join([random.choice(opts) for x in range(l)])
    
#     return outStr
