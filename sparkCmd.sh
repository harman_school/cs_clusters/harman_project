#!/bin/bash

### --------  SLURM  ----------- ###
#SBATCH --job-name=sparkReddit
#SBATCH --time=10:00:00
#SBATCH -p map-reduce
#SBATCH --nodes=15
#SBATCH --ntasks=15
### -------------------------- ###

pyfile=/cslu/homes/harmang/harman_project/preproc_FindTopPosts.py

module load hadoop

spark-submit --master=yarn --num-executors 15 --driver-memory 8G --executor-memory 8G ${pyfile}

# Add one over here too
pyspark --master=yarn --num-executors 25 --driver-memory 25g --executor-memory 25G
# Add one over here too