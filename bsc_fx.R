

runRF = function(df, filt1, filt2){
    """ Function to partition data and run random forest """
    
    dff = df %>% filter(label == 1 | postLabel_Hope == 'o_negativeSituation'); dim(dff)
    #dff = df
    # Merge dataset 
    dd = merge(dfFilt, dff, by = 'id') %>%
        select(-id, -postLabel_Hope, -postLabel_harmang, -title)

    # Refactor albel
    dd$label = as.factor(ifelse(dd$label == 1, 's', 'o'))

    # Create nFolds
    nFolds = 10
    folds = createFolds(dd$label, k = nFolds)

    auc_vect = c()

    # Train at each fold
    for (ii in names(folds)){

        currInds = folds[[ii]]

        # Setup test and train sets
        test = dd[currInds, ]
        testX = select(test, -label)
        testY = test$label

        train = dd[-currInds, ]

        # Run the random forest
        rf.mod = randomForest(label ~ .,
                                data = train,
                                xtest = testX,
                                ytest = testY,
                                mtry = round(sqrt(ncol(train))),
                                keep.forest = TRUE,
                                importance = TRUE,
                                ntree = 500)

        pp = confusionMatrix(rf.mod$test$predicted, test$label)
        print(paste('AUC: ', pp$byClass[[11]]))

        auc_vect[[length(auc_vect)+1]] = pp$byClass[[11]]

    }

    return (auc_vect)

}