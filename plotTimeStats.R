
# Load libraries 
library(ggplot2)
library(dplyr)
library(sjPlot)

parDir = '//Users/harmang/Desktop/git_home/coursework/CS_clusters/harman_project/'
theme_set(theme_blank()) # Set theme

# Read the time stats file
dfTime = read.csv(paste0(parDir, 'timeStats.csv'))
dfTime = dfTime %>%
  filter(docs < 5100 & docs > 10)
# Plot
ggplot(dfTime, aes(x = docs, y = time, color = alg)) + 
  geom_point() + 
  geom_line() + 
  xlab('Reddit Posts') + 
  ylab('Time [s]') + 
  ggtitle('Time Comparison Native vs Spark') + 
  theme(plot.title = element_text(hjust = 0.5))